# node.js 应用程序核心

[![Npm version](https://img.shields.io/npm/v/node-app-core.svg)](https://www.npmjs.com/package/node-app-core)

node.js 非常便于开发微服务的项目，npm 不乏针对具体服务场景的框架和类库，但非常缺乏将 node.js 的应用程序的启动、目录结构组织成一个有效的、可复用的架构。

这并不是一件易于实现的事情，毕竟事实上每个 node.js 的应用程序环境千差万别。

该类库也只基于自己多年开发 node.js 微服务所涉及的经验，排除应用中所需的具体类库，只针对 app 的初始化、启动。

### 安装说明

```
yarn add node-app-core
npm install node-app-core --save
```

### 使用说明

目前我们将应用程序的启动，界定为三个阶段：

1. 应用程序实例化（new App）阶段。
2. 加载应用程序的环境变量（initEnv）阶段 —— 指当前运行模式，附加的自定义配置文件等。
3. 应用程序正式启动（bootstrap）阶段 —— 根据环境变量，生成对应的配置文件，并加载配置到应用程序的属性中。

理论上，会是如下的流程：

```javascript
const AppCore = require('node-app-core');

const appRoot = 'any where'; // 如果不指定，默认为 process.cwd()
const appOptions = { // 应用程序的启动配置参数，以下列举为默认值
    envFile             : '.env', // 默认的环境变量文件，基于 appRoot ，可以指定为绝对路径
    configDir           : 'config', // 默认的配置路径，基于 appRoot ，可以指定为绝对路径
    isAppendCustomConfig: true, // 是否允许追加自定义配置文件
    configFiles         : ['common.json'], // 默认要加载的配置文件
    // 还有一个 mergeOptions ，是决定 app 的 merge 方法的逻辑，请参考 npm/deepmerge 
}; 

const app = new AppCore(appRoot, appOptions);

app.initEnv(); // 这一步骤可以省略，将包含在 bootstarp 中
// 输出环境变量的内容
// console.log(app.envVars) 
// 输出当前环境名称
// console.log(app.env) 

app.bootstrap();
// console.log(app.config) 
```

而实际上在项目中，推荐的做法是：

```javascript
// app.js
const AppCore = require('node-app-skeleton');

class MyWebServiceApp extends AppCore {

    onConstructor() {
        // 应用程序实例化时
    }
    
    onBootstrap() {
        // 应用程序启动时
    }
}

const globalApp = new MyWebServiceApp;
globalApp.bootstrap();

module.exports = globalApp;
```

在其他的应用程序文件中：

```javascript
const app = require('./app.js');

// ....

```