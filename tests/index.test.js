const pathUtil = require('path');
const fs = require('fs');
const url = require('url');

const deepmerge = require('deepmerge');

const AppCore = require('../index.js');

describe('AppCore', () => {

	const cwd = process.cwd(), os = process.platform, isWin = os === 'win32';
	const pathOf = (path) => pathUtil.resolve(__dirname, path);

	const app1Root = pathOf('app1');
	const app1CustomEnvFile = 'app1_env.json';

	const newApp = (path, options) => new AppCore(path, options);

	it('new app properties test', () => {
		const app = new AppCore(app1Root);

		expect(app.root).toBe(app1Root);
		expect(app.cwd).toBe(cwd);
		expect(app.os).toBe(os);
		expect(app.isWindows).toBe(isWin);
		expect(app.dirBasename).toBe(pathUtil.basename(app1Root));
		expect(app.isBootstrap).toBe(false);
		expect(app.getEnvFile()).toBe(pathOf('app1/.env'));
		expect(app.loadEnv()).toMatchObject({
			env: AppCore.DEVEL,
		});
		expect(app.getConfigFiles()).toContain('common.json'); // 默认的公共配置
		expect(app.getConfigRoot()).toBe(pathOf('app1/config'));
	});

	it('change options test', () => {
		// envFile 指定一个绝对路径
		const ops1 = {envFile: pathOf(app1CustomEnvFile)};
		const app1 = newApp(app1Root, ops1);
		expect(app1.getEnvFile()).toBe(pathOf(app1CustomEnvFile));
		expect(app1.env).toBe(AppCore.PROD);

		// 去掉默认的配置文件
		const ops2 = {configFiles: null};
		expect(newApp(app1Root, ops2).getConfigFiles()).not.toContain('common.json');

		const app3 = newApp(app1Root);
		app3.initEnv();
		expect(app3.getConfigFiles()).toContain('common.json');
		expect(app3.getConfigFiles()).toContain('development.json');
		expect(app3.getConfigFiles()).toContain('custom.json');
		expect(app3.getConfigFiles().length).toBe(3);

		const app3_1 = newApp(app1Root, {configFiles: null, isAppendCustomConfig: false});
		app3_1.initEnv();
		expect(app3_1.getConfigFiles()).not.toContain('common.json');
		expect(app3_1.getConfigFiles()).toContain('development.json');
		expect(app3_1.getConfigFiles()).not.toContain('custom.json');
		expect(app3_1.getConfigFiles().length).toBe(1);

		// 将 configDir 改为一个绝对路径
		const ops4 = {configDir: pathOf('app1_config')};
		expect(newApp(app1Root, ops4).getConfigRoot()).toBe(pathOf('app1_config'));


	});

	it('convertPath test', () => {
		const app = newApp(app1Root);

		expect(app.convertPath(undefined)).toBe('');
		expect(app.convertPath(null)).toBe('');

		const url1 = new url.URL('http://www.oschina.net/');
		expect(app.convertPath(url1)).toBe('http://www.oschina.net/');

		const url2 = url.parse('http://www.oschina.net/');
		expect(app.convertPath(url1)).toBe('http://www.oschina.net/');

		expect(app.convertPath(pathOf('app1_env.json'))).toBe(pathOf('app1_env.json'));

		expect(app.convertPath('.env')).toBe(pathOf('app1/.env'));
	});

	it('loadFile test', () => {
		const app = newApp(app1Root);

		const file1 = app.loadFile('hello_world.txt');
		expect(file1.buffer).not.toBe(undefined);
		expect(file1.buffer.toString()).toBe('hello_world');

		const file2 = app.loadFile('test1.txt');
		expect(file2.buffer).not.toBe(undefined);
		expect(file2.buffer.toString()).toBe('test1');

		const file3 = app.loadFile('not-exists.txt');
		expect(file3.err).not.toBe(undefined);
	});

	it('loadFileAsString test', () => {
		const app = newApp(app1Root);

		const file1 = app.loadFileAsString('hello_world.txt');
		expect(file1).toBe('hello_world');

		const file2 = app.loadFileAsString('test1.txt');
		expect(file2).toBe('test1');

		const file3 = app.loadFileAsString('not-exists.txt');
		expect(file3).toBe('');

		const file4 = app.loadFileAsString('test2.txt');
		expect(file4).toBe('test2');
	});

	it('merge', () => {
		const app = newApp(app1Root);

		const a1 = ['a1-0', 'a1-1', 'a1-2'];
		const a2 = ['a2-0', 'a2-1'];

		const m1 = app.merge(a1, a2);
		expect(m1).toMatchObject(['a2-0', 'a2-1', 'a1-2']);

		const m2 = app.merge(a2, a1);
		expect(m2).toMatchObject(['a1-0', 'a1-1', 'a1-2']);

		const o1 = {
			a: ['o1-a0', 'o1-a1', 'o1-a2'],
		};

		const o2 = {
			a: ['o2-a0', 'o2-a1'],
		};

		const m3 = app.merge(o1, o2);
		expect(m3.a).toMatchObject(['o2-a0', 'o2-a1', 'o1-a2']);

		const o3 = {
			a: {
				a1: 'a1',
				a3: 'a3',
			},
		};

		const o4 = {
			a: {
				a1: 'a1-replace',
				a2: 'a2',
			},
		};

		const m4 = app.merge(o3, o4);
		expect(m4).toMatchObject({
			a: {
				a1: 'a1-replace',
				a2: 'a2',
				a3: 'a3',
			},
		});
	});

	it('parseJsonAsObject test', () => {
		const json1 = `{"a": "a", "b": "b", "c": {"c1": "c1", "c2": "c2"}}`;

		const app = newApp(app1Root);

		expect(app.parseJsonAsObject(json1)).toMatchObject({
			a: 'a',
			b: 'b',
			c: {
				c1: 'c1',
				c2: 'c2',
			},
		});

		const json2 = `{}`;

		expect(app.parseJsonAsObject(json2, {b: 'b-replace', c: {c3: 'c3'}})).toMatchObject({
			b: 'b-replace',
			c: {
				c3: 'c3',
			},
		});

		expect(app.parseJsonAsObject(() => {
			return {a: 1, b: 2};
		})).toMatchObject({
			a: 1,
			b: 2,
		});

		expect(app.parseJsonAsObject({
			a: 1,
			b: 2,
		}, {c: 'c'})).toMatchObject({
			a: 1,
			b: 2,
			c: 'c',
		});
	});

	it('load config', () => {
		const app1 = newApp(app1Root);
		app1.initEnv();
		expect(app1.loadConfig()).toMatchObject({
			key1 : 'key1-custom',
			array: ['a-custom', 'b-custom', 'c-common'],
			obj  : {
				a: 'a-custom',
				b: 'b-custom',
				c: 'c-common',
			},
		});

		app1.bootstrap();
		expect(app1.config).toMatchObject({
			key1 : 'key1-custom',
			array: ['a-custom', 'b-custom', 'c-common'],
			obj  : {
				a: 'a-custom',
				b: 'b-custom',
				c: 'c-common',
			},
		});


	});
});

